"""
TODO
"""
import sys
import numpy as np
from scipy.io import wavfile
try:
    import simpleaudio as sa
except ModuleNotFoundError:
    pass


def play_audio(audio_signal: np.ndarray,
               sample_rate: int = 16000,
               num_channels: int = 1,
               bytes_per_sample: int = 2) -> None:
    """
    TODO
    """
    play_obj: sa.PlayObject = sa.play_buffer(audio_data=audio_signal,
                                             num_channels=num_channels,
                                             bytes_per_sample=bytes_per_sample,
                                             sample_rate=sample_rate)
    play_obj.wait_done()


def to_float32(signal: np.ndarray) -> np.ndarray:
    """
    Cast data (typically from WAV) to float32.

    Source:
    https://github.com/LCAV/pyroomacoustics/blob/218c0ec3e8422f1ede30de684520c782a500f9ff/pyroomacoustics/utilities.py#L129

    Args:
        signal (np.ndarray):    Real signal in time domain, typically obtained from WAV file.

    Returns:
        signal (np.ndarray):    `signal` as float32.
    """
    if np.issubdtype(signal.dtype, np.integer):
        max_val: int = abs(np.iinfo(signal.dtype).min)
        return signal.astype(np.float32) / max_val

    return signal.astype(np.float32)


def main() -> None:
    assert len(sys.argv) == 2

    sample_rate: int
    audio_signal: np.ndarray
    sample_rate, audio_signal = wavfile.read(sys.argv[1])

    print('Shape:', audio_signal.shape)

    play_audio(audio_signal=audio_signal,
               sample_rate=sample_rate)


if __name__ == '__main__':
    main()
