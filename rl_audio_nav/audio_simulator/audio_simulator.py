"""
DEPRECATED
"""

from typing import Optional
from multiprocessing.connection import Connection

from .room import Room


class AudioSimulator:

    def __init__(self,
                 simulator_agent_pipe: Connection,
                 simulator_ui_pipe: Optional[Connection] = None) -> None:
        """
        TODO

        Args:
            simulator_agent_pipe (Connection):  TODO.
            simulator_ui_pipe (Connection):     TODO.
        """
        self.room: Room

        self.simulator_agent_pipe: Connection = simulator_agent_pipe
        self.simulator_ui_pipe: Optional[Connection] = simulator_ui_pipe

    def run(self) -> None:
        pass


def run_simulator(**kwargs) -> None:
    """
    TODO
    """
    audio_simulator: AudioSimulator = AudioSimulator(**kwargs)
    audio_simulator.run()
