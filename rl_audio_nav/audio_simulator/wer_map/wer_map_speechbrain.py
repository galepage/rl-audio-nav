#!/usr/bin/env python3

import logging
from time import time

import numpy as np
import exputils as eu

from rl_audio_nav import asr
from rl_audio_nav.asr import AsrDecoder, SpeechBrainAsr
from rl_audio_nav.audio_simulator import AudioSignal, SpeechSignal
from rl_audio_nav.audio_simulator.room import Room, GpuRirRoom, Source

LOGGER: logging.Logger = logging.getLogger(__name__)

DEFAULT_CONFIG: eu.AttrDict = eu.AttrDict()


def compute_wer_map(dataset: list[SpeechSignal],
                    config: eu.AttrDict = None,
                    **kwargs) -> np.ndarray:

    config = eu.combine_dicts(kwargs, config, DEFAULT_CONFIG)

    # Create room
    room: Room = GpuRirRoom(config=config.room)

    # Initialize the microphone array
    room.init_grid(add_grid_mics=True)

    n_mics: int = len(room.microphones)
    LOGGER.info("grid size: %ix%i = %i microphones", room.n_x, room.n_y, n_mics)

    # Initialize ASR recognizer
    asr_decoder: AsrDecoder = SpeechBrainAsr(use_gpu=True)

    num_samples: int = len(dataset)
    LOGGER.info("num_samples: %i", num_samples)

    speech_source: Source = Source(name='speech',
                                   location=np.array([config.speech_x,
                                                      config.speech_y,
                                                      1.8]))
    room.add_source(speech_source)

    if config.noise:
        noise_signal: AudioSignal = AudioSignal(name='noise',
                                                file_path='data/guitar_16k.wav')

        noise_source: Source = Source(name='noise',
                                      location=np.array([config.noise_x,
                                                         config.noise_y,
                                                         1.8]))
        room.add_source(noise_source)
        noise_signal.load_audio()
        room.set_source_input_audio_signal(input_audio_signal=noise_signal.signal,
                                           source_name='noise')

        noise_source2: Source = Source(name='noise2',
                                       location=np.array([2, 1, 1.8]))
        room.add_source(noise_source2)
        room.set_source_input_audio_signal(input_audio_signal=noise_signal.signal,
                                           source_name='noise2')

    # Pre compute the RIR
    start_time: float = time()
    room.pre_compute_rir()
    logging.info("Done computing RIR (took %fs)",
                 time() - start_time)

    wer_matrix: np.ndarray = np.zeros(shape=(room.n_x, room.n_y))
    LOGGER.debug('wer_matrix %s', wer_matrix.shape)

    # For each data sample from the data set add the corresponding speech source.
    # with alive_bar(num_samples) as progress_bar:

    for speech_signal in dataset:
        speech_signal.load_audio()

#         asr_result: str = asr_decoder.decode(signal=speech_signal.signal)
#
#         wer: float = asr.compute_wer(ground_truth=speech_signal.transcript,
#                                      hypothesis=asr_result)

        room.set_source_input_audio_signal(input_audio_signal=speech_signal.signal,
                                           source_name='speech')

        wer_list: list[float] = []

        # For each grid point get the signal and get the WER at this location.
        for mic_index in range(n_mics):
            # print(f"{mic_index}/{n_mics}")
            listened_signal: np.ndarray = room.get_audio_at_mic(mic_index=mic_index,
                                                                save=True,
                                                                save_dir=config.samples_save_dir)

            asr_result = asr_decoder.decode(signal=listened_signal)

            wer = asr.compute_wer(ground_truth=speech_signal.transcript,
                                  hypothesis=asr_result)

            # Save the mean WER in the matrix.
            wer_list.append(wer)

        assert len(wer_list) == room.n_x * room.n_y
        wer_matrix += np.array(wer_list).reshape((room.n_x, room.n_y))

        LOGGER.debug("mean WER for %s = %f",
                     speech_signal.filename,
                     np.mean(wer_list))

        # pylint: disable=not-callable
        # progress_bar()

    # Normalize WER matrix by the number of samples.
    wer_matrix /= num_samples

    return wer_matrix
