"""
Dereverberation module.
"""
import matplotlib.pyplot as plt
import numpy as np
import soundfile as sf
from tqdm import tqdm

from nara_wpe.wpe import wpe
from nara_wpe.wpe import get_power
from nara_wpe.utils import stft, istft, get_stft_center_frequencies

import pyroomacoustics as pra

from rl_audio_nav.audio_simulator.utils import play_audio


def main():
    channels = 8
    sampling_rate = 16000
    delay = 3
    iterations = 5
    taps = 10
    alpha=0.9999

    stft_options = dict(size=512, shift=128)

    file_template = 'AMI_WSJ20-Array1-{}_T10c0201.wav'
    signal_list = [
        sf.read('data/nara_wpe/' + file_template.format(channel_index + 1))[0]
        for channel_index in range(channels)
    ]
    signal = np.stack(signal_list, axis=0)
    # IPython.display.Audio(y[0], rate=sampling_rate)

    plt.plot(signal[0])
    plt.plot(np.average(signal, axis=0))
    plt.show()
    play_audio(audio_signal=pra.utilities.normalize(np.average(signal, axis=0),
                                                    bits=16),
               sample_rate=sampling_rate,
               num_channels=1)

    # STFT
    stft_signal = stft(signal, **stft_options).transpose(2, 0, 1)

    stft_post_wpe = wpe(Y=stft_signal,
                        taps=taps,
                        delay=delay,
                        iterations=iterations,
                        statistics_mode='full').transpose(1, 2, 0)

    clean_signal = istft(stft_signal=stft_post_wpe,
                         size=stft_options['size'],
                         shift=stft_options['shift'])
    # IPython.display.Audio(z[0], rate=sampling_rate)


if __name__ == '__main__':
    main()
