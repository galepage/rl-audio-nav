"""
Noise dataset management.
"""

from os.path import join
from typing import Iterable, Iterator
import csv
import numpy as np

from rl_audio_nav.audio_simulator.room.room import Source
from rl_audio_nav import asr
from rl_audio_nav.audio_simulator.audio_signal import AudioSignal, SpeechSignal


class RandomNoiseSource(Source):

    def __init__(self,
                 name: str,
                 location: np.ndarray,
                 snr: float) -> None:
        super().__init__(name=name,
                         location=location)

        self.snr = snr

    @property
    def signal(self):
        pass

        # Scale noise


def noise_data_set_iterator() -> Iterator[AudioSignal]:
    data_set_path: str = 'data/fma/'

    with open(join(data_set_path, 'fma_metadata/tracks.csv'), 'r') as tracks_metadata:
        csv_reader = csv.reader(tracks_metadata, delimiter=',')
        yield next(csv_reader)
        yield next(csv_reader)
        yield next(csv_reader)
        # for track in csv_reader:
        #     yield track

#     for speaker_dir in os.listdir(data_set_path):
#
#         speaker_id: str = path.basename(speaker_dir)
#
#         speaker_dir = path.join(data_set_path, speaker_dir)
#
#         for chapter_dir in os.listdir(speaker_dir):
#
#             chapter_id: str = path.basename(chapter_dir)
#
#             chapter_dir = path.join(speaker_dir, chapter_dir)
#
#             with open(path.join(chapter_dir,
#                                 f"{speaker_id}-{chapter_id}.trans.txt"), 'r') as trans_file:
#
#                 trans_file_lines: List[str] = trans_file.readlines()
#
#             audio_file_names: List[str] = [filename
#                                            for filename in os.listdir(chapter_dir)
#                                            if filename.endswith('.wav')]
#             audio_file_names.sort()
#
#             for sample_index, (trans_line, audio_filename) \
#                     in enumerate(zip(trans_file_lines, audio_file_names)):
#
#                 trans_line_split: List[str] = trans_line.split()
#
#                 base_name: str = f"{speaker_id}-{chapter_id}-{sample_index:04d}"
#
#                 assert trans_line_split[0] == base_name
#
#                 assert path.basename(audio_filename) == base_name + '.flac.wav'
#
#                 audio_filename = path.join(chapter_dir, audio_filename)
#
#                 transcript: str = ' '.join(trans_line_split[1:])
#
#                 yield SpeechSignal(file_path=audio_filename,
#                                    transcript=transcript)

    return


def compute_snr(signal: np.ndarray,
                noise: np.ndarray) -> float:

    signal_power: float = np.sum(signal ** 2) / len(signal)
    noise_power: float = np.sum(noise ** 2) / len(noise)
    return 10 * np.log10(signal_power / noise_power)


def scale_noise(speech_signal: np.ndarray,
                noise_signal: np.ndarray,
                target_snr: float) -> np.ndarray:

    power_speech: float = np.sum(speech_signal ** 2) / len(speech_signal)
    power_noise: float = np.sum(noise_signal ** 2) / len(noise_signal)
    coefficient: float = np.exp(np.log10(power_speech / power_noise) - target_snr / 10)

    return (noise_signal * coefficient).astype('int16')


def test_noise_scaling() -> None:
    from scipy.io import wavfile

    speech_dataset: Iterable[SpeechSignal] = asr.speech_data_set_iterator()

    speech_signal_obj = next(speech_dataset)
    speech_signal_obj.load_audio()
    speech_signal: np.ndarray = speech_signal_obj.signal

    _, noise_signal = wavfile.read(filename='data/guitar_16k.wav')

    print('speech signal type:', speech_signal.signal.dtype)
    print('noise signal type:', noise_signal.dtype)

    print('no scaling:')
    # play_audio(audio_signal=noise_signal, sample_rate=sampling_freq)
    print('snr =', compute_snr(signal=speech_signal, noise=noise_signal))

    import matplotlib.pyplot as plt
    plt.plot(speech_signal)
    plt.show()

    for snr in (-5, 0, 5):
        print('target_snr = ', snr)
        noise_signal = scale_noise(speech_signal=speech_signal,
                                   noise_signal=noise_signal,
                                   target_snr=snr)
        print(noise_signal.dtype)
        wavfile.write(filename=f'output_{snr}.wav',
                      rate=16000,
                      data=noise_signal)
        # play_audio(audio_signal=noise_signal)
        print('computed snr =', compute_snr(signal=speech_signal, noise=noise_signal))


def main() -> None:

    import rl_audio_nav.audio_simulator.fma_utils as utils

    tracks = utils.load('data/fma/data/fma_metadata/tracks.csv')
    print(tracks.shape)


if __name__ == '__main__':
    main()
