#!/usr/bin/env python3

"""
Main programme to test the audio simulator.
"""

from alive_progress import alive_bar

from rl_audio_nav import asr
from rl_audio_nav.audio_simulator.room import Room, Source
# from rl_audio_nav.audio_simulator.room import PyRoomAcousticsRoom
from rl_audio_nav.audio_simulator.room import GpuRirRoom
from rl_audio_nav.audio_simulator.audio_signal import SpeechSignal


N_STEPS: int = 1000
N_SOURCES: int = 20


def main() -> None:
    """
    Test the simulator.
    """
    room: Room = GpuRirRoom()
    # room: Room = PyRoomAcousticsRoom()
    room.init_grid(add_grid_mics=True,
                   delta_x=0.1,
                   delta_y=0.1)

    dataset: list[SpeechSignal] = asr.get_speech_data_set(random=False,
                                                          num_samples=N_STEPS)

    # TODO benchmark and compare those two settings:
    # - loop through the samples and then through the grid locations (like we actually do in)
    # - loop through the grid locations and then through the samples

    for source_index in range(N_SOURCES):
        room.add_source(source=Source(name=str(source_index),
                                      location=room.get_random_position()))

    with alive_bar(N_STEPS) as progress_bar:
        for speech_signal in dataset:

            speech_signal.load_audio()

            for source_index in range(N_SOURCES):
                room.move_source(source_name=str(source_index),
                                 new_location=room.get_random_grid_position())

                room.set_source_input_audio_signal(input_audio_signal=speech_signal.signal,
                                                   source_name=str(source_index))

            # room.simulate()
            room.pre_compute_rir()

            progress_bar()


if __name__ == '__main__':
    main()
