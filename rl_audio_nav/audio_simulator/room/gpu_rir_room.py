"""
Room using the GPU RIR backend.
"""

import numpy as np
import gpuRIR
import exputils as eu

from .room import Room


class GpuRirRoom(Room):
    """

    Attributes:
        TODO
    """

    def __init__(self,
                 config: eu.AttrDict = None,
                 **kwargs) -> None:
        super().__init__(config=config,
                         **kwargs)

        gpuRIR.activateMixedPrecision(False)
        gpuRIR.activateLUT(True)

        # Size of the room [m]
        room_dim: np.ndarray = self.get_room_dims()

        # Absortion coefficient ratios of the walls
        abs_weights: list[float] = [0.9] * 5 + [0.5]

        # Attenuation when start using the diffuse reverberation model [dB]
        att_diff: float = 15.0

        # Attenuation at the end of the simulation [dB]
        att_max: float = 60.0

        # Reflection coefficients
        self.beta: np.ndarray = gpuRIR.beta_SabineEstimation(room_sz=room_dim,
                                                             T60=self.rt_60,
                                                             abs_weights=abs_weights)

        # Time to start the diffuse reverberation model [s]
        self.time_diffuse_reverb: float = gpuRIR.att2t_SabineEstimator(att_dB=att_diff,
                                                                       T60=self.rt_60)
        if self.max_time == -1:
            self.max_time = gpuRIR.att2t_SabineEstimator(att_dB=att_max,
                                                         T60=self.rt_60)
        # Number of image sources in each dimension
        self.nb_img: list[int] = gpuRIR.t2n(T=self.time_diffuse_reverb,
                                            rooms_sz=room_dim)

    def pre_compute_rir(self) -> None:
        """
        Pre compute the RIR for every source-microphone pair.
        """
        import gpuRIR

        if self.rir_up_to_date:
            self.logger.debug("RIR is already up to date: returning.")
            return
        self.logger.debug("Computing RIR")

        # Size of the room [m]
        room_dim: np.ndarray = np.array([self.size_x, self.size_y, self.height])

        assert len(self.sources) > 0

        sources_positions: np.ndarray = np.array([source.location
                                                  for source in self.sources.values()])

        mic_positions: np.ndarray = np.array([mic.location
                                              for mic in self.microphones.values()])

        mic_orientations: np.ndarray = np.array([mic.orientation
                                                 for mic in self.microphones.values()])

        mic_pattern: str = self.get_mic_from_index(index=0).pattern

        self.rir: np.ndarray = gpuRIR.simulateRIR(room_sz=room_dim,
                                                  beta=self.beta,
                                                  pos_src=sources_positions,
                                                  pos_rcv=mic_positions,
                                                  nb_img=self.nb_img,
                                                  Tmax=self.max_time,
                                                  fs=self.sampling_frequency,
                                                  Tdiff=self.time_diffuse_reverb,
                                                  orV_rcv=mic_orientations,
                                                  mic_pattern=mic_pattern)

        # We have to transpose the result to have microphones on the first axis and sources on the
        # second axis.
        self.rir = np.transpose(a=self.rir,
                                axes=[1, 0, 2])

        self.rir_up_to_date = True
