"""
TODO
"""

import logging
from os.path import basename

import numpy as np
from scipy.io import wavfile
import matplotlib.pyplot as plt
import torch
import torchaudio

from . import utils


class AudioSignal:

    def __init__(self,
                 file_path: str,
                 name: str = '',
                 load_audio_array: bool = False,
                 load_audio_tensor: bool = False) -> None:
        self._logger: logging.Logger = logging.getLogger(self.__class__.__name__)

        self.name: str = name
        self.file_path: str = file_path
        self.filename: str = basename(file_path)

        self.signal: np.ndarray
        self.sample_rate: int
        self.signal_tensor: torch.Tensor
        self.signal_shape: tuple
        self.signal_dim: int

        if load_audio_array:
            self.load_audio()

        if load_audio_tensor:
            self.load_audio_tensor()

    def load_audio(self) -> None:
        if hasattr(self, 'signal') and self.signal is not None:
            # self._logger.debug('signal was already loaded, skipping.')
            return

        self.sample_rate, self.signal = wavfile.read(filename=self.file_path)
        self.signal_shape = self.signal.shape
        self.signal_dim = self.signal.ndim

    def load_audio_tensor(self) -> None:
        if hasattr(self, 'signal_tensor') and self.signal_tensor is not None:
            # self._logger.debug('signal_tensor was already loaded, skipping.')
            return

        self.signal_tensor, self.sample_rate = torchaudio.load(filepath=self.file_path)
        self.signal_tensor = self.signal_tensor.squeeze()
        self.signal_shape = self.signal_tensor.shape
        self.signal_dim = self.signal_tensor.dim()

    @property
    def tensor_signal(self) -> torch.Tensor:
        if self.signal_tensor is None:
            self.load_audio_tensor()

        return self.signal_tensor

    @property
    def normalized_signal(self) -> np.ndarray:
        return utils.to_float32(signal=self.signal)

    @property
    def n_channels(self) -> int:
        if self.signal_dim > 1:
            return self.signal_shape[0]

        # Mono signal
        return 1

    @property
    def n_samples(self) -> int:
        if not hasattr(self, 'signal_shape') or self.signal_shape is None:
            raise AttributeError("You have to load the signal before asking for the number of"
                                 " samples.")
        return self.signal_shape[-1]

    @property
    def duration(self) -> float:
        return self.n_samples / self.sample_rate

    def is_mono(self) -> bool:
        return self.n_channels == 1

    def is_stereo(self) -> bool:
        return self.n_channels == 2

    def play(self) -> None:
        utils.play_audio(audio_signal=self.signal,
                         sample_rate=self.sample_rate,
                         num_channels=self.n_channels,
                         bytes_per_sample=2)

    def save(self, path: str) -> None:
        wavfile.write(filename=path,
                      rate=self.sample_rate,
                      data=self.signal)

    def plot(self) -> None:
        # TODO
        pass

    @staticmethod
    def plot_signals(signals: list['AudioSignal']) -> plt.plot:

        for signal in signals:
            if signal.signal is None:
                signal.load_audio()

            times: np.ndarray = np.arange(start=0,
                                          stop=signal.duration)

            plt.plot(times,
                     signal.signal,
                     label=signal.name)

        plt.legend()
        plt.show()

    def __str__(self) -> str:
        # TODO
        return ''

    def __repr__(self) -> str:
        # TODO
        return ''


class SpeechSignal(AudioSignal):
    """
    TODO
    """

    def __init__(self,
                 file_path: str,
                 transcript: str,
                 load_audio_array: bool = False,
                 load_audio_tensor: bool = False) -> None:
        super().__init__(file_path=file_path,
                         load_audio_array=load_audio_array,
                         load_audio_tensor=load_audio_tensor)

        self.transcript: str = transcript
