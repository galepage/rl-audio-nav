#!/usr/bin/env python3

"""
TODO
"""

import sys
from argparse import ArgumentParser, Namespace
from typing import Any
from multiprocessing import Process, Pipe

from rl_audio_nav.ui import run_ui
from rl_audio_nav.audio_simulator import run_simulator
from rl_audio_nav.agent import run_agent


def parse_args(args: list[str]) -> Namespace:
    """
    Parse the arguments from the command line.

    Args:
        args (list[str]):   The arguments from the command line call.

    Returns:
        parser (Namespace):    Populated namespace.
    """
    parser = ArgumentParser(description="Evaluate panel segmentation detections.")

    parser.add_argument('--ui',
                        help="Run the user interface.",
                        action='store_true')

    return parser.parse_args(args)


def main(args: list[str] = None):
    """
    Launch evaluation of the panel segmentation task on a JSON data set.

    Args:
        args (list[str]):   Arguments from the command line.
    """
    # Parse arguments.
    if args is None:
        args = sys.argv[1:]
    parsed_args: Namespace = parse_args(args)

    simulator_args: dict[str, Any] = {}

    agent_simulator_pipe, simulator_agent_pipe = Pipe()

    if parsed_args.ui:
        # Create a pipe for the simulator and the UI to communicate with each other
        simulator_ui_pipe, ui_simulator_pipe = Pipe()
        simulator_args['simulator_ui_pipe'] = simulator_ui_pipe

        # Create the UI node
        ui_args: dict[str, Any] = {'ui_simulator_pipe': ui_simulator_pipe}
        ui_process: Process = Process(target=run_ui,
                                      kwargs=ui_args)

        # Run the UI node
        ui_process.start()

    # Create the simulator node
    simulator_args['simulator_agent_pipe'] = simulator_agent_pipe
    simulator_process: Process = Process(target=run_simulator,
                                         kwargs=simulator_args)

    # Start the simulator node
    simulator_process.start()

    agent_args: dict[str, Any] = {
        'agent_simulator_pipe': agent_simulator_pipe
    }
    agent_process: Process = Process(target=run_agent,
                                     kwargs=agent_args)

    agent_process.start()


if __name__ == '__main__':
    main()
