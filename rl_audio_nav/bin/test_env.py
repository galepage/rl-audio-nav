"""
Script to test the RL environment
"""

import gym
import numpy as np

from ..rl.environments import RlAudioNavEnv


def main() -> None:

    env: gym.Env = RlAudioNavEnv()

    observation: np.ndarray
    reward: float
    done: bool
    info: dict

    for _ in range(100):

        env.render()

        action: np.ndarray = env.action_space.sample()

        observation, reward, done, info = env.step(action=action)


if __name__ == '__main__':
    main()
