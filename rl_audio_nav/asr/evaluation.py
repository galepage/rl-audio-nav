"""
Functions to evaluate the performance of Automatic Speech Recognition (ASR).
"""

import os
from typing import Mapping
import jiwer


def wer_jiwer(real_file_path: str,
              est_file_path: str,
              result_file_path: str) -> tuple[list[str], list[str]]:
    """
    DEPRECATED (or at list I don't use it right now)
    """
    with open(real_file_path, 'r', errors='replace') as real_file:
        with open(est_file_path, 'r', errors='replace') as est_file:
            real_list: list[str] = []
            est_list: list[str] = []
            for _ in range(100):
                real_list.append(real_file.readline())
                est_list.append(est_file.readline().replace('’', ''))

    transformation: jiwer.Compose = jiwer.Compose([jiwer.RemoveMultipleSpaces(),
                                                   jiwer.Strip(),
                                                   jiwer.ToLowerCase(),
                                                   jiwer.RemovePunctuation()])

    measures: Mapping[str, float] = jiwer.compute_measures(truth=transformation(real_list),
                                                           hypothesis=transformation(est_list))

    with open(result_file_path, 'a') as result_file:
        result_file.write('JIWER results:\n')
        result_file.write(f"WER: {measures['wer']}\n")
        result_file.write(f"MER: {measures['mer']}\n")
        result_file.write(f"WIL: {measures['wil']}\n")

    return transformation(est_list), transformation(real_list)


def wer_cmd(real_file_path: str,
            est_file_path: str,
            result_file_path: str) -> None:
    """
    DEPRECATED (or at list I don't use it right now)
    """
    command: str = f'wer -a -i {real_file_path} {est_file_path} >> {result_file_path}'
    os.system(command)


def compute_wer(ground_truth: str,
                hypothesis: str) -> float:
    """
    Compute the word error rate (WER) for one sentence.

    Args:
        ground_truth (str):     Ground truth transcript for the sentence.
        hypothesis (str):       Output of the ASR model.

    Returns:
        wer (float):     word error rate (as a floating point number).
    """
    transformation: jiwer.Compose = jiwer.Compose([jiwer.RemoveMultipleSpaces(),
                                                   jiwer.Strip(),
                                                   jiwer.ToLowerCase(),
                                                   jiwer.RemovePunctuation()])
    ground_truth = transformation(ground_truth)
    hypothesis = transformation(hypothesis)

    # print('GT:', ground_truth)
    # print('HYP:', hypothesis)

    return jiwer.wer(truth=ground_truth,
                     hypothesis=hypothesis)


def main() -> None:
    real_file_path: str = 'data/test/facebookDB/real.txt'
    est_file_path: str = 'out/test/decode_google.out'
    result_file_path: str = './results.txt'

    try:
        os.remove(result_file_path)
    except FileNotFoundError:
        pass

    clean_est, clean_real = wer_jiwer(real_file_path=real_file_path,
                                      est_file_path=est_file_path,
                                      result_file_path=result_file_path)

    wer_cmd(real_file_path=real_file_path,
            est_file_path=est_file_path,
            result_file_path=result_file_path)

    clean_est_file_path: str = 'clean_est.txt'
    with open(clean_est_file_path, 'w', errors='replace') as clean_est_file:
        clean_est_file.write('\n'.join(clean_est))

    clean_real_file_path: str = 'clean_real.txt'
    with open(clean_real_file_path, 'w', errors='replace') as clean_real_file:
        clean_real_file.write('\n'.join(clean_real))

    wer_cmd(real_file_path=clean_real_file_path,
            est_file_path=clean_est_file_path,
            result_file_path=result_file_path)


if __name__ == '__main__':
    main()
