#!/usr/bin/env python3

import logging
from typing import Union
from multiprocessing import Process, cpu_count, Manager
from threading import Thread

from alive_progress import alive_bar
from rl_audio_nav.asr import AsrDecoder, VoskAsr

from rl_audio_nav import asr
from rl_audio_nav.audio_simulator import SpeechSignal


# MODE: str = 'multiprocessing'
MODE: str = 'multithreading'
NUM_JOBS: int = 6

# `-1` means "all samples"
# NUM_SAMPLES: int = -1
NUM_SAMPLES: int = 10
USE_GPU: bool = True
RANDOM: bool = True

LOGGER: logging.Logger = logging.getLogger(__name__)


def evaluate_asr(dataset: list[SpeechSignal],
                 results_dict: dict,
                 job_index: int) -> None:
    """
    TODO

    Args:
        dataset (list[SpeechSignal]):   A list of speech signals.
        results_dict (dict):            The dict where to store the WER:
                                            `results_dict[job_index] = wer`
        job_index (int):                The job index.
    """
    num_samples: int = len(dataset)
    # Initialize ASR recognizer
    asr_decoder: AsrDecoder = VoskAsr(use_gpu=USE_GPU,
                                      in_thread=MODE == 'multithreading')

    total_wer: float = 0

    LOGGER.debug("job#%i | num_samples: %i", job_index, num_samples)

    speech_signal: SpeechSignal
    # with alive_bar(num_samples) as progress_bar:
    for index, speech_signal in enumerate(dataset):

        speech_signal.load_audio()

        asr_result: str = asr_decoder.decode(signal=speech_signal.signal)

        wer: float = asr.compute_wer(ground_truth=speech_signal.transcript,
                                     hypothesis=asr_result)

        total_wer += wer

            # progress_bar()

    results_dict[job_index] = total_wer / num_samples


def main():
    # If using multithreading, we need to init the GPU in the main process.
    VoskAsr.init_vosk(init_gpu=USE_GPU and MODE == 'multithreading')

    dataset: list[SpeechSignal] = asr.get_speech_data_set(random=RANDOM,
                                                          seed=0,
                                                          load_audio_array=True,
                                                          num_samples=NUM_SAMPLES)
    num_jobs: int = NUM_JOBS
    if num_jobs < 0:
        if MODE == 'multiprocessing':
            num_jobs = cpu_count()
        else:
            raise ValueError(f"wrong value for NUM_JOBS:{num_jobs}")

    LOGGER.info("num_jobs = %i", num_jobs)

    num_samples: int = len(dataset)
    LOGGER.info("total number of samples: %i", num_samples)
    num_samples_per_job: int = int(num_samples // num_jobs)
    LOGGER.info("num_samples_per_job: %i", num_samples_per_job)

    result_dict: dict[int, float]
    multi_object: Union[Process, Thread]
    if MODE == 'multiprocessing':
        result_dict = Manager().dict()
        multi_object = Process
    elif MODE == 'multithreading':
        result_dict = dict()
        multi_object = Thread

    jobs: list[multi_object] = []

    for job_index in range(num_jobs):

        # Split the data set.
        start_index: int = job_index * num_samples_per_job
        end_index: int = (job_index + 1) * num_samples_per_job
        sub_dataset: list[SpeechSignal] = dataset[start_index:end_index]

        # Create a 'job'
        job: multi_object = multi_object(target=evaluate_asr,
                                         args=(sub_dataset, result_dict, job_index))

        jobs.append(job)

        job.start()

    for job in jobs:
        job.join()

    total_wer: float = sum(result_dict.values())

    mean_wer: float = total_wer / num_jobs
    LOGGER.info("mean wer: %.2f%%", 100 * mean_wer)


if __name__ == '__main__':
    main()
