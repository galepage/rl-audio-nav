"""
LibriSpeech data set loader.
"""

import os
from os import path
import random as rd
from typing import Iterator
import logging

from alive_progress import alive_bar

from rl_audio_nav.audio_simulator.audio_signal import SpeechSignal

LOGGER: logging.Logger = logging.getLogger(__name__)

NUM_SAMPLES: int = 28_539


def data_set_length() -> int:
    """
    Returns:
        data_set_length (int):  The number of samples in the complete data set.
    """
    return len(get_speech_data_set(random=False))


def _data_set_loading_loop(keep_indices: list[int],
                           load_audio_array: bool,
                           load_audio_tensor: bool) -> list[SpeechSignal]:

    data_set_path: str = 'data/LibriSpeech/train-clean-100/'
    LOGGER.info("Loading dataset '%s'", data_set_path)

    num_samples: int = len(keep_indices)

    def _load_data_set(progress_bar=None) -> list[SpeechSignal]:
        data_set: list[SpeechSignal] = []

        next_sample_to_keep: int = keep_indices.pop(0)
        sample_global_index: int = 0

        # Loop through speakers
        for speaker_dir in os.listdir(data_set_path):

            speaker_id: str = path.basename(speaker_dir)

            speaker_dir = path.join(data_set_path, speaker_dir)

            # Loop through chapters
            for chapter_dir in os.listdir(speaker_dir):

                chapter_id: str = path.basename(chapter_dir)

                chapter_dir = path.join(speaker_dir, chapter_dir)

                with open(path.join(chapter_dir,
                                    f"{speaker_id}-{chapter_id}.trans.txt"), 'r') as trans_file:

                    trans_file_lines: list[str] = trans_file.readlines()

                audio_file_names: list[str] = [filename
                                               for filename in os.listdir(chapter_dir)
                                               if filename.endswith('.wav')]
                audio_file_names.sort()

                # Loop through samples
                for sample_index, (trans_line, audio_filename) \
                        in enumerate(zip(trans_file_lines, audio_file_names)):

                    if sample_global_index == next_sample_to_keep:

                        trans_line_split: list[str] = trans_line.split()

                        base_name: str = f"{speaker_id}-{chapter_id}-{sample_index:04d}"

                        assert trans_line_split[0] == base_name

                        assert path.basename(audio_filename) == base_name + '.flac.wav'

                        audio_filename = path.join(chapter_dir, audio_filename)

                        transcript: str = ' '.join(trans_line_split[1:])

                        data_set.append(SpeechSignal(file_path=audio_filename,
                                                     transcript=transcript,
                                                     load_audio_array=load_audio_array,
                                                     load_audio_tensor=load_audio_tensor))

                        if progress_bar is not None:
                            progress_bar()

                        if len(keep_indices) == 0:
                            return data_set
                        next_sample_to_keep = keep_indices.pop(0)

                    sample_global_index += 1

        return data_set

    if not load_audio_array and not load_audio_tensor:
        return _load_data_set()

    else:
        with alive_bar(num_samples) as progress_bar:
            data_set: list[SpeechSignal] = _load_data_set(progress_bar=progress_bar)

        return data_set


def get_speech_data_set(random: bool = True,
                        seed: int = None,
                        num_samples: int = NUM_SAMPLES,
                        load_audio_array: bool = False,
                        load_audio_tensor: bool = False) -> list[SpeechSignal]:

    if seed is not None:
        rd.seed(seed)
    LOGGER.info("num_samples: %i", num_samples)
    LOGGER.info("random: %s" + (f" (seed = {seed})" if random else ''), random)
    LOGGER.info("load_audio_array: %s", load_audio_array)
    LOGGER.info("load_audio_tensor: %s", load_audio_tensor)
    if num_samples < 0:
        num_samples = NUM_SAMPLES

    keep_indices: list[int] = list(range(NUM_SAMPLES))
    if num_samples != NUM_SAMPLES:
        if random:
            LOGGER.debug("Loading %i/%i samples", num_samples, NUM_SAMPLES)
            keep_indices = rd.sample(keep_indices, k=num_samples)
            keep_indices.sort()
        else:
            keep_indices = list(range(num_samples))
    else:
        LOGGER.info("Loading all the %i samples", NUM_SAMPLES)

    data_set: list[SpeechSignal] = _data_set_loading_loop(keep_indices=keep_indices,
                                                          load_audio_array=load_audio_array,
                                                          load_audio_tensor=load_audio_tensor)

    if random:
        rd.shuffle(data_set)

    return data_set


def speech_data_set_iterator(random: bool = True) -> Iterator[SpeechSignal]:
    for speech_signal in get_speech_data_set(random=random):
        yield speech_signal


def main() -> None:
    """
    Simply loop through the data set and print information about each sample.
    """
    for speech_signal in get_speech_data_set(random=False):

        LOGGER.info("audio filename: %s", speech_signal.filename)
        LOGGER.info("transcript: %s", speech_signal.transcript)


if __name__ == '__main__':
    main()
