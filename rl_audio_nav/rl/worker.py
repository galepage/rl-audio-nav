from typing import Any
from threading import Thread
from queue import Queue
from collections import namedtuple

import gym
import exputils as eu


WorkerMessage: namedtuple = namedtuple('WorkerMessage',
                                       ['cmd', 'data'])


def _worker_process(in_queue: Queue,
                    out_queue: Queue,
                    env_name: str,
                    env_config: eu.AttrDict = None) -> None:
    env_kwargs: dict[str, Any] = {}
    if env_config is not None:
        env_kwargs['config'] = env_config
    env: gym.Env = gym.make(env_name,
                            **env_kwargs)

    message: WorkerMessage
    while True:
        message = in_queue.get()
        if message.cmd == 'step':
            out_queue.put(env.step(action=message.data))

        elif message.cmd == 'reset':
            out_queue.put(env.reset())

        elif message.cmd == 'stop':
            break
        else:
            raise NotImplementedError


class Worker:

    def __init__(self,
                 env_name: str,
                 env_config: eu.AttrDict) -> None:

        self._in_queue: Queue = Queue()
        self.out_queue: Queue = Queue()
        self._thread: Thread = Thread(target=_worker_process,
                                      args=(self._in_queue,
                                            self.out_queue,
                                            env_name,
                                            env_config))

        self._thread.start()

    def reset(self) -> None:
        self._in_queue.put(WorkerMessage(cmd='reset',
                                         data=None))

    def step(self, action) -> None:
        self._in_queue.put(WorkerMessage(cmd='step',
                                         data=action))

    def stop(self) -> None:
        self._in_queue.put(WorkerMessage(cmd='stop',
                                         data=None))

        # The thread should stop
        self._thread.join()
