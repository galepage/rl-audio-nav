from rl_audio_nav.rl.agent.ppo.ppo import PPO


def main() -> None:

    ppo: PPO = PPO()

    ppo.run_training_loop()


# Run it
if __name__ == "__main__":
    main()
