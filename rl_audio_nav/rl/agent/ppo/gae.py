"""
Generalized Advantage Estimation (GAE)

This is a [PyTorch](https://pytorch.org) implementation of paper
[Generalized Advantage Estimation](https://arxiv.org/abs/1506.02438).
"""

import numpy as np


class GAE:
    """
    Generalized Advantage Estimation (GAE)
    """

    def __init__(self,
                 n_workers: int,
                 n_worker_steps: int,
                 gamma: float,
                 lambda_: float) -> None:

        self.lambda_: float = lambda_
        self.gamma: float = gamma
        self.n_worker_steps: int = n_worker_steps
        self.n_workers: int = n_workers

    def __call__(self,
                 dones: np.ndarray,
                 rewards: np.ndarray,
                 values: np.ndarray) -> np.ndarray:
        """
        Compute advantages.
        N: self.n_workers
        T: self.n_worker_steps

        Args:
            dones (np.ndarray):         A vector of boolean telling, for each environment and each
                                            time step if the episode is over.
                                            Shape=(T, N)
            rewards (np.ndarray):       The reward values for each time step.
                                            Shape=(T, N)
            values: (np.ndarray):       The estimated values for the encountered states.
                                            Shape=(T, N)

        Returns:
            advantages (np.ndarray):    The advantage values for the trajectories.
                                            Shape=(T, N)
        """
        # advantages table
        # shape: (T, N)
        advantages: np.ndarray = np.zeros(shape=(self.n_worker_steps, self.n_workers),
                                          dtype=np.float32)
        # A_T = 0
        last_advantage: np.ndarray = np.zeros(shape=(self.n_workers,),
                                              dtype=np.float32)

        # V(s_{t+1})
        # Initialization: V(s_T)
        # shape: (N,)
        last_value: np.ndarray = values[-1]
        mask: np.ndarray

        # for t = [T-1, T-2, ..., 0]
        for time_step in reversed(range(self.n_worker_steps)):
            # mask if episode completed after step time_step
            mask = 1.0 - dones[time_step]

            # V(s_t+1)
            last_value *= mask

            # A_t+1
            last_advantage *= mask

            # delta_t = r_t + gamma * V(s_t+1) - V(s_t)
            delta = rewards[time_step] + self.gamma * last_value - values[time_step]

            # A_t = delta_t + gamma * lambda * A_t+1
            last_advantage = delta + self.gamma * self.lambda_ * last_advantage

            # note that we are collecting in reverse order.
            # *My initial code was appending to a list and
            #   I forgot to reverse it later.
            # It took me around 4 to 5 hours to find the bug.
            # The performance of the model was improving
            #  slightly during initial runs,
            #  probably because the samples are similar.*
            # A[t] = A_t
            advantages[time_step] = last_advantage

            # V_t = V[t]
            last_value = values[time_step]

        return advantages
