"""
---
title: PPO Experiment with Atari Breakout
summary: Annotated implementation to train a PPO agent on Atari Breakout game.
---

# PPO Experiment with Atari Breakout

This experiment trains Proximal Policy Optimization (PPO) agent  Atari Breakout game on OpenAI Gym.
It runs the [game environments on multiple processes](../game.html) to sample efficiently.
"""

import logging
from typing import Sequence

import exputils as eu

import numpy as np
import torch
from torch import nn
from torch import Tensor
from torch.distributions import Categorical, Distribution
from torch.nn.utils.rnn import PackedSequence, pack_padded_sequence

from ...environments.rl_audio_nav_env import ACTION_NAMES
from ..policy import Policy, get_action_from_policy


class PPOAgent(nn.Module, Policy):
    """
    Model
    """

    default_config: eu.AttrDict = eu.AttrDict(
        # The number of input feature at each time step
        num_audio_embeddings=129,

        # The feeature size outputed by the recurrent layer
        rnn_output_size=128
    )

    def __init__(self,
                 config: eu.AttrDict,
                 device: torch.device = torch.device('cpu'),
                 **kwargs) -> None:

        super().__init__()

        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)

        self.device: torch.device = device

        self.config: eu.AttrDict = eu.combine_dicts(kwargs,
                                                    config,
                                                    self.default_config)

        # RNN
        self.recurrent_layer: nn.RNNBase = nn.LSTM(input_size=self.config.num_audio_embeddings,
                                                   hidden_size=self.config.rnn_output_size,
                                                   batch_first=True)

        # Common dense layers
        self.linear_1: nn.Linear = nn.Linear(in_features=self.config.rnn_output_size,
                                             out_features=self.config.rnn_output_size)

        self.activation_1: nn.ReLU = nn.ReLU()

        self.linear_2: nn.Linear = nn.Linear(in_features=self.config.rnn_output_size,
                                             out_features=64)

        self.activation_2: nn.ReLU = nn.ReLU()

        # Policy head
        self.pi_logits: nn.Linear = nn.Linear(in_features=64,
                                              out_features=len(ACTION_NAMES))

        # Value head
        self.value: nn.Linear = nn.Linear(in_features=64,
                                          out_features=1)

    def forward(self,
                obs: Tensor,
                obs_lengths: Tensor = None) -> tuple[Categorical, Tensor]:

        # Case where no length vector was given.
        # If it is for inference on a single sequence, no worry, we can simply put 1.
        if obs_lengths is None:
            assert obs.ndim == 2

            obs_lengths = torch.tensor([1.0],
                                       dtype=torch.int64,
                                       device='cpu')

        # Observation Tensor shape: (B, F, T)
        #   B: batch_size
        #   F: embedding_dim
        #   T: seq_len (max sequence length)

        # (B, F, T) --> (B, T, F)
        obs = torch.transpose(input=obs,
                              dim0=1,
                              dim1=2)

        #######
        # RNN #
        #######
        packed_obs: PackedSequence = pack_padded_sequence(input=obs,
                                                          lengths=obs_lengths,
                                                          batch_first=True,
                                                          enforce_sorted=False)

        h_n: Tensor
        # The output of a LSTM is `output, (h_n, c_n)`
        # i.e. a `tuple[Union[Tensor, PackedSequence], tuple[Tensor, Tensor]]`
        _, (h_n, _) = self.recurrent_layer(packed_obs)

        # shape: (B, E) (this operation removes the dimension equal to 1)
        h_n = torch.squeeze(h_n)

        #######################
        # Common dense layers #
        #######################

        # Dense layers common to value and policy
        temp: Tensor = self.linear_1(h_n)
        temp = self.activation_1(temp)

        temp = self.linear_2(temp)
        temp = self.activation_2(temp)

        ###############
        # Policy head #
        ###############

        # shape: (B, |A|)
        logits: Tensor = self.pi_logits(temp)

        policy: Categorical = Categorical(logits=logits)

        ##############
        # value head #
        ##############

        # shape: (B,)
        value: torch.Tensor = self.value(temp).squeeze()

        return policy, value

    @torch.no_grad()
    def select_action(self,
                      observation: np.ndarray,
                      evaluate: bool = False) -> int:
        """
        Select an action knowing the state of the environment
        and according to the current policy.

        Args:
            obs (np.ndarray):   state of the environment
            evaluate (bool):    If true, take most probable action despite of sampling.

        Returns:
            action (int):    The action taken by the policy.
        """
        obs_tensor: Tensor = torch.from_numpy(np.float32(observation)).to(self.device)
        policy: Distribution = self(obs_tensor)[0]

        return int(get_action_from_policy(policy=policy,
                                          evaluate=evaluate))

    @torch.no_grad()
    def select_actions(self,
                       observations: Sequence[np.ndarray],
                       evaluate: bool = False) -> np.ndarray:
        # TODO, finish this, boy
        # Each `obs` has shape (F, L_n), n in (1, N)
        obs_seq_lengths: np.ndarray = np.array([obs.shape[-1] for obs in observations])
        obs_lengths: Tensor = torch.tensor(obs_seq_lengths,
                                           dtype=torch.int64,
                                           device='cpu')

        max_obs_seq_len: int = np.max(obs_seq_lengths)

        obs_array: np.ndarray = np.zeros(shape=(len(observations),
                                                observations[0].shape[0],
                                                max_obs_seq_len),
                                         dtype=np.float32)

        # Padding
        for obs_index, obs in enumerate(observations):
            obs_len: int = obs.shape[-1]

            obs_array[obs_index, :, :obs_len] = obs

        obs_tensor: Tensor = torch.tensor(obs_array, device=self.device)

        policy: Distribution = self(obs_tensor, obs_lengths)[0]

        return get_action_from_policy(policy=policy,
                                      evaluate=evaluate)
