import logging
from typing import Any, Iterable
from threading import Thread
from queue import Queue
import sys
from enum import Enum
from copy import deepcopy

import numpy as np
import exputils as eu
import gym
from gym import logger
from gym.error import (
    AlreadyPendingCallError,
    NoAsyncCallError,
    ClosedEnvironmentError,
)

from vosk import SetLogLevel, GpuInit
from .rl_audio_nav_env import RlAudioNavEnv


class AsyncState(Enum):
    DEFAULT = 'default'
    WAITING_RESET = 'reset'
    WAITING_STEP = 'step'


class Worker:
    def __init__(self,
                 index: int,
                 env_config: eu.AttrDict,
                 error_queue: Queue) -> None:

        # print(f'Hello, I am thread {index}')
        self._env: gym.Env = RlAudioNavEnv(config=env_config,
                                           in_thread=True)
        # print('env done init')

        self.index: int = index

        self._input_queue: Queue = Queue()
        self._output_queue: Queue = Queue()
        self._error_queue: Queue = error_queue

        self._thread: Thread = Thread(target=self._run_thread,
                                      name=f"Worker-thread-{index:02}")

        self._thread.start()

    def reset_async(self) -> None:
        self._input_queue.put(('reset', None))

    def reset_wait(self) -> tuple[np.ndarray, bool]:
        observation: np.ndarray
        success: bool
        observation, success = self._output_queue.get()

        return observation, success

    def step_async(self, action: Any) -> None:
        self._input_queue.put(('step', action))

    def step_wait(self) -> tuple[tuple[np.ndarray,
                                       float,
                                       bool,
                                       dict],
                                 bool]:

        result: tuple[np.ndarray, float, bool, dict]
        success: bool
        result, success = self._output_queue.get()

        return result, success

    def check_observation_space(self, space: gym.Space) -> tuple[bool, bool]:
        """
        Checks that the provided gym space is the same as the worker's environment one's.

        Args:
            space (gym.Space):  A candidate gym space

        Returns:
            same_space (bool):  True if the provided space is the same as the one in the worker's
                                    environment.
        """
        self._input_queue.put(('_check_observation_space', space))

        return self._output_queue.get()

    def _run_thread(self) -> None:
        command: str
        data: Any
        try:
            while True:
                command, data = self._input_queue.get()

                if command == 'reset':
                    self._output_queue.put((self._env.reset(), True))

                elif command == 'step':
                    observation, reward, done, info = self._env.step(data)

                    if done:
                        observation = self._env.reset()
                    self._output_queue.put(((observation, reward, done, info), True))

                elif command == 'seed':
                    self._env.seed(seed=data)
                    self._output_queue.put((None, True))

                elif command == 'close':
                    self._output_queue.put((None, True))
                    break

                elif command == '_check_observation_space':
                    self._output_queue.put((data == self._env.observation_space, True))

                else:
                    raise RuntimeError(f"Received unknown command `{command}`."
                                       "Must be one of {`reset`, `step`, `seed`, `close`, "
                                       "`_check_observation_space`}.")

            # pylint: disable=broad-except
        except (KeyboardInterrupt, Exception):
            self._error_queue.put((self.index,) + sys.exc_info()[:2])
            self._output_queue.put((None, False))
        finally:
            self._env.close()

    def close(self) -> None:
        self._env.close()
        self._input_queue.put(('close', None))


class RlAudioNavVecEnv(gym.vector.VectorEnv):

    def __init__(self,
                 num_envs: int,
                 env_config: eu.AttrDict = eu.AttrDict(),
                 copy: bool = True) -> None:

        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)

        super().__init__(num_envs=num_envs,
                         observation_space=RlAudioNavEnv.observation_space,
                         action_space=RlAudioNavEnv.action_space)

        self.observations: list[np.ndarray] = []

        self.copy: bool = copy

        if env_config.enable_gpu_asr:
            # Initialize GPU for ASR
            self.logger.info('Initializing GPU for ASR `GpuInit()`')
            SetLogLevel(0)
            GpuInit()

        self.error_queue: Queue = Queue()

        self.workers: list[Worker] = [Worker(index=worker_index,
                                             env_config=env_config,
                                             error_queue=self.error_queue)
                                      for worker_index in range(self.num_envs)]

        self._state: AsyncState = AsyncState.DEFAULT

    def reset_async(self) -> None:
        self._assert_is_running()
        if self._state != AsyncState.DEFAULT:
            raise AlreadyPendingCallError("Calling `reset_async` while waiting for a pending call"
                                          f" to `{self._state.value}` to complete",
                                          self._state.value)

        for worker in self.workers:
            worker.reset_async()
        self._state = AsyncState.WAITING_RESET

    def reset_wait(self) -> list[np.ndarray]:  # pylint: disable=arguments-differ
        """
        Reset the environment and block until the operation is over.

        Args:
            timeout (int):  Number of seconds before the call to `reset_wait` times out.
                                If `None`, the call to `reset_wait` never times out.

        Returns:
            observations (list[np.ndarray]):    Sample from `observation_space` A batch of
                                                    observations from the vectorized environment.
        """
        self._assert_is_running()
        if self._state != AsyncState.WAITING_RESET:
            raise NoAsyncCallError(
                "Calling `reset_wait` without any prior " "call to `reset_async`.",
                AsyncState.WAITING_RESET.value)

        results, successes = zip(*[worker.reset_wait() for worker in self.workers])
        self._raise_if_errors(successes)
        self._state = AsyncState.DEFAULT

        self.observations = list(results)

        return deepcopy(self.observations) if self.copy else self.observations

    def step_async(self, actions: list[Any]) -> None:
        """
        Args:
            actions (list[Any]):    list of samples from `action_space`.
        """
        self._assert_is_running()
        if self._state != AsyncState.DEFAULT:
            raise AlreadyPendingCallError(f"Calling `step_async` while waiting for a pending call"
                                          f" to `{self._state.value}` to complete.",
                                          self._state.value)

        for action, worker in zip(actions, self.workers):
            worker.step_async(action=action)
        self._state = AsyncState.WAITING_STEP

    # pylint: disable=arguments-differ
    def step_wait(self) -> tuple[list[np.ndarray],
                                 np.ndarray,
                                 np.ndarray,
                                 list[dict]]:
        """
        Args:
            timeout (int):  Number of seconds before the call to `step_wait` times out.
                                If `None`, the call to `step_wait` never times out.

        Returns:
            observations (list[np.ndarray]):    A batch of observations from the vectorized
                                                    environment.
            rewards (np.ndarray):               `np.ndarray` instance (dtype `np.float_`)
                                                    A vector of rewards from the vectorized
                                                    environment.
            dones (np.ndarray):                 A vector whose entries indicate whether the episode
                                                    has ended.
            infos (list[dict]):                 A list of auxiliary diagnostic information.
        """
        self._assert_is_running()
        if self._state != AsyncState.WAITING_STEP:
            raise NoAsyncCallError("Calling `step_wait` without any prior call " "to `step_async`.",
                                   AsyncState.WAITING_STEP.value)

        results: Iterable[tuple[np.ndarray, float, bool, dict]]
        successes: Iterable[bool]
        results, successes = zip(*[worker.step_wait() for worker in self.workers])

        self._raise_if_errors(successes)
        self._state = AsyncState.DEFAULT

        observations_list: Iterable[np.ndarray]
        rewards: Iterable[float]
        dones: Iterable[bool]
        infos: Iterable[dict]
        observations_list, rewards, dones, infos = zip(*results)

        # `zip` yields tuples so we need to convert it to a list
        self.observations = list(observations_list)

        return (deepcopy(self.observations) if self.copy else self.observations,
                np.array(rewards),
                np.array(dones, dtype=np.bool_),
                list(infos))

    def render(self, mode: str = 'human') -> None:
        pass

    # pylint: disable=arguments-differ
    def close_extras(self) -> None:
        """
        Close all the environments.
        """
        for worker in self.workers:
            worker.close()

    def _check_observation_spaces(self) -> None:
        self._assert_is_running()

        same_spaces: Iterable[bool]
        successes: Iterable[bool]
        same_spaces, successes = zip(*[
            worker.check_observation_space(space=self.single_observation_space)
            for worker in self.workers
        ])
        self._raise_if_errors(successes=successes)
        if not all(same_spaces):
            raise RuntimeError("Some environments have an observation space different from"
                               f" `{self.single_observation_space}`. In order to batch"
                               " observations, the observation spaces from all environments must be"
                               " equal.")

    def _assert_is_running(self) -> None:
        if self.closed:
            raise ClosedEnvironmentError(
                f"Trying to operate on `{type(self).__name__}`, after a call to `close()`.")

    def _raise_if_errors(self,
                         successes: Iterable[bool]) -> None:
        if all(successes):
            return

        num_errors: int = self.num_envs - sum(successes)
        assert num_errors > 0
        for _ in range(num_errors):
            index: int
            exctype: type
            value: BaseException
            index, exctype, value = self.error_queue.get()
            logger.error(f"Received the following error from Worker-{index}: "
                         f"{exctype.__name__}: {value}")

            logger.error(f"Shutting down Worker-{index}.")
            self.workers[index].close()
            self.workers[index] = None  # type: ignore

        logger.error("Raising the last exception back to the main process.")
        raise exctype(value)
