from gym.envs.registration import register

register(id='rl_audio_nav_env-v0',
         entry_point='rl_audio_nav.rl.environments:RlAudioNavEnv')
