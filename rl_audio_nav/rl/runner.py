"""
Run policy in environment (policy agnostic)
"""
import logging
from typing import Any

import numpy as np
import gym
import exputils as eu


class Runner:
    """
    Run a policy over a gym environment to test it.
    """
    def __init__(self,
                 env: gym.Env,
                 render: bool = False,
                 max_number_of_steps_per_episode: int = -1) -> None:

        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)
        self.env: gym.Env = env

        self.render: bool = render

        self.max_number_of_steps_per_episode: int = max_number_of_steps_per_episode

    def test(self,
             policy,
             n_episodes: int) -> tuple[float, float]:
        """
        Peform a total of `n_episodes` episodes on the parallel environments using the provided
        policy.

        Args:
            policy (todo):          The policy implementation that will be used to select actions.
                                        it needs to implement the `select_action(actions)` method.
            n_episodes (int):       The total number of episodes to perform.
            max_step (int):         The max number of steps per episode.

        returns:
            mean_reward (float):    The mean of the collected reward.
            std_reward (float):     The standard of the collected reward.
        """
        all_episodes_cumulated_rewards: np.ndarray = np.zeros(n_episodes)
        all_episodes_mean_rewards: np.ndarray = np.zeros(n_episodes)

        observation: np.ndarray
        reward: float
        done: bool

        for episode_index in range(n_episodes):
            self.logger.info("test episode n°%i/%i",
                             episode_index + 1,
                             n_episodes)
            step_counter: int = 0
            done = False
            episode_cumulated_reward: float = 0

            stop: bool = False

            # reset the environment
            observation = self.env.reset()

            while not stop:
                self.logger.info("episode step n°%i/%i",
                                 step_counter + 1,
                                 self.max_number_of_steps_per_episode)
                if self.render:
                    self.env.render()

                action: int = policy.select_action(observation=observation,
                                                   evaluate=True)

                observation, reward, done, _ = self.env.step(action)

                episode_cumulated_reward += reward
                self.logger.debug("episode_cumulated_reward: %s", episode_cumulated_reward)

                step_counter += 1

                stop = done or (self.max_number_of_steps_per_episode > 0
                                and step_counter >= self.max_number_of_steps_per_episode)

            all_episodes_cumulated_rewards[episode_index] = episode_cumulated_reward

            all_episodes_mean_rewards[episode_index] = episode_cumulated_reward / step_counter

        self.logger.debug("all_episodes_mean_rewards: %s", all_episodes_mean_rewards)
        self.logger.debug("all_episodes_cumulated_rewards: %s", all_episodes_cumulated_rewards)

        return np.mean(all_episodes_cumulated_rewards), np.std(all_episodes_cumulated_rewards)


class VecEnvRunner(Runner):
    """
    Run a policy over a set of gym environments (i.e. a vectorized environment) to test it.
    """

    def __init__(self,
                 vec_env: gym.vector.VectorEnv,
                 max_number_of_steps_per_episode: int = -1) -> None:

        super().__init__(env=vec_env,
                         max_number_of_steps_per_episode=max_number_of_steps_per_episode)

        # Number of individual environments.
        self.num_envs: int = self.env.num_envs

    def test(self,
             policy,
             n_episodes: int) -> tuple[float, float]:
        """
        Peform a total of `n_episodes` episodes on the parallel environments using the provided
        policy.

        Args:
            policy (TODO):          The policy implementation that will be used to select actions.
                                        It needs to implement the `select_action(actions)` method.
            n_episodes (int):       The total number of episodes to perform.
            max_step (int):         The max number of steps per episode.

        Returns:
            mean_reward (float):    The mean of the collected reward.
            std_reward (float):     The standard of the collected reward.
        """
        assert n_episodes % self.num_envs == 0
        all_episodes_cumulated_rewards: np.ndarray = np.zeros(n_episodes)
        all_episodes_mean_rewards: np.ndarray = np.zeros(n_episodes)

        # The real number of episode per worker.
        n_episodes //= self.num_envs

        # observations is a list of `self.num_envs` observations of shape (F, L_n)
        observations: list[np.ndarray]
        dones: np.ndarray
        step_rewards: np.ndarray

        for episode_index in range(n_episodes):
            self.logger.info("Test episode n°%i/%i",
                             episode_index + 1,
                             n_episodes)
            all_envs_done: bool = False

            step_counter: int = 0
            all_envs_done = False
            episode_cumulated_rewards: np.ndarray = np.zeros(self.num_envs)
            episode_lengths: np.ndarray = np.zeros(self.num_envs)

            stop: bool = False

            # reset the environments
            observations = self.env.reset()

            while not stop:
                self.logger.info("Episode step n°%i/%i",
                                 step_counter + 1,
                                 self.max_number_of_steps_per_episode)
                actions: np.ndarray = policy.select_actions(observations=observations,
                                                            evaluate=True)

                observations, step_rewards, dones, _ = self.env.step(actions)
                all_envs_done = bool(np.all(dones))
                self.logger.debug("Dones: %s", dones)

                # same problem as below: we have to count the last step.
                episode_lengths += 1 - dones

                # Mask the rewards (ensure rewards of completed envs are 0)
                # TODO: we should not mask the reward from the last step (although `done` is True !)
                # We may compute the mask before 'stepping' in the environment...
                # Look at how it is done in gae
                # step_rewards = step_rewards * (1 - dones)
                episode_cumulated_rewards += step_rewards
                self.logger.debug("episode_cumulated_rewards: %s", episode_cumulated_rewards)

                step_counter += 1

                stop = all_envs_done or (self.max_number_of_steps_per_episode > 0
                                         and step_counter >= self.max_number_of_steps_per_episode)

            all_episodes_cumulated_rewards[episode_index:episode_index + self.num_envs] = \
                episode_cumulated_rewards

            all_episodes_mean_rewards[episode_index:episode_index + self.num_envs] = \
                episode_cumulated_rewards / episode_lengths

        self.logger.debug("all_episodes_mean_rewards: %s", all_episodes_mean_rewards)
        self.logger.debug("all_episodes_cumulated_rewards: %s", all_episodes_cumulated_rewards)

        return np.mean(all_episodes_cumulated_rewards), np.std(all_episodes_cumulated_rewards)


class OldRunner:
    """
    Run a policy over a gym environment to test it.
    """

    def __init__(self,
                 env_name: str,
                 env_config: eu.AttrDict = None) -> None:
        env_kwargs: dict[str, Any] = {}
        if env_config is not None:
            env_kwargs['env_config'] = env_config
        self.env: gym.Env = gym.make(env_name,
                                     **env_kwargs)

    def run(self,
            policy,
            env_step=0,
            num_episodes=0) -> None:
        # TODO: add an assert conditions on env_step and num_episodes
        if env_step == 0:
            for _ in range(num_episodes):
                state = self.env.reset()
                done = False
                while not done:
                    action = policy.select_action(state)
                    next_state, _, done, _ = self.env.step(action)
                    # replay_buffer.add(state, action, reward, next_state)
                    state = next_state
        else:
            state = self.env.reset()
            done = False
            for _ in range(env_step):
                if done:
                    state = self.env.reset()
                action = policy.select_action(state)
                next_state, _, done, _ = self.env.step(action)
                state = next_state

    def test(self,
             policy,
             n_iterations,
             max_step=0) -> tuple[float, float]:
        list_reward: list[float] = []
        for _ in range(n_iterations):
            state = self.env.reset()
            done: bool = False
            sum_reward: float = 0
            if max_step == 0:
                while not done:
                    action = policy.select_action(obs=state,
                                                  evaluate=True)
                    next_state, reward, done, _ = self.env.step(action)
                    sum_reward += reward
                    state = next_state
            else:
                for _ in range(max_step):
                    # print("state", state)
                    action = policy.select_action(obs=state,
                                                  evaluate=True)
                    # print("action", action)
                    next_state, reward, done, _ = self.env.step(action)
                    sum_reward += reward
                    state = next_state
                    if done:
                        break

            list_reward.append(sum_reward)
        return np.mean(list_reward), np.std(list_reward)

    def render(self,
               policy,
               max_step: int) -> None:
        state = self.env.reset()
        self.env.render()
        done: bool = False
        reward: float = 0
        sum_reward: float = 0
        for step in range(max_step):
            action = policy.select_action(state)
            next_state, reward, done, _ = self.env.step(action)
            sum_reward += reward
            self.env.render()
            state = next_state
            if done:
                break
        print(f"Episode over {step}/{max_step} steps. Reward: {sum_reward}")
        self.env.close()

    def run_random(self,
                   n_iterations: int,
                   max_step: int = 0) -> tuple[float, float]:

        list_reward: list[float] = []
        for _ in range(n_iterations):
            self.env.reset()
            done = False
            sum_reward: float = 0
            if max_step == 0:
                while not done:
                    action = self.env.action_space.sample()
                    _, reward, done, _ = self.env.step(action)
                    sum_reward += reward
            else:
                for _ in range(max_step):
                    action = self.env.action_space.sample()
                    _, reward, done, _ = self.env.step(action)
                    sum_reward += reward

                    if done:
                        break

            list_reward.append(sum_reward)

        return np.mean(list_reward), np.std(list_reward)
