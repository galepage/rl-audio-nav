#!/bin/sh


# HOSTNAME='alya'
HOSTNAME='chamaeleon'
PORT=9999

if lsof -i -P -n | grep $PORT 2>&1 > /dev/null; then
    echo "ssh bridge is already running"
else
    echo "starting ssh bridge"
    ssh -N -L $PORT:localhost:$PORT -J bastion galepage@$HOSTNAME.inrialpes.fr &
fi

# Start the server remotely
remi desktop command -b -c -h $HOSTNAME "python rl_audio_nav/gui/server.py"

sleep 5

# Start the client
python rl_audio_nav/gui/gui.py

echo TOTO
