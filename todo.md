# TODO list RL audio RL audio nav

## Audio simulator

### Common

- [x] Implement the generic Room class.

### PyRoomAcoustics

- [ ] Compare ISM alone with ISM + RT

### GpuRIR


## ASR

- [x] Loop over the Librispeach data set and compute WER


## UI

- [ ] client-server communication
- [ ] Room UI (plot the room, the sources and the grid)
- [ ] WER map plot (in BG)
- [ ] Click2Listen
- [ ] Trajectory replay


## Common

- [ ] Compute WER at different locations
- [ ] Create a launcher for the simulator

## RL

Interesting implementations:
- RL:
    - Gym environments:
        - [How to create new environments for Gym](https://github.com/openai/gym/blob/master/docs/creating-environments.md)
        - Gym source code: [GitHub](https://github.com/openai/gym/tree/master):
            - [`gym.Env`](https://github.com/openai/gym/blob/master/gym/core.py)
            - [`gym.Space`](https://github.com/openai/gym/blob/master/gym/spaces/space.py)
            - [Vector Environments](https://github.com/openai/gym/tree/master/gym/vector)
                - [`VectorEnv`](https://github.com/openai/gym/blob/master/gym/vector/vector_env.py)
                - [`SyncVectorEnv`](https://github.com/openai/gym/blob/master/gym/vector/sync_vector_env.py)
                - [`AsyncVectorEnv`](https://github.com/openai/gym/blob/master/gym/vector/async_vector_env.py)
                - Tristan Deleu's doc for gym vector environments: [GitHub](https://tristandeleu.github.io/gym/vector/), [PR](https://github.com/openai/gym/pull/2327/commits/6ad2ef92fcaa8995ba4e6eb924c1d561385c20d1)
        - [Most basic RL loop ever](https://github.com/DerwenAI/gym_example/blob/master/sample.py)
        - [Stable Baselines 3's **vectorized environments**:](https://stable-baselines3.readthedocs.io/en/master/guide/vec_envs.html)
    - TD3: simple, not inventing itself a life. [GitHub](https://github.com/sfujim/TD3)
    - OpenAI baselines: [`ppo2.py`](https://github.com/openai/baselines/blob/master/baselines/ppo2/ppo2.py)
    - Stable baselines 3 [GitHub](https://github.com/sfujim/TD3) - [Doc](https://stable-baselines3.readthedocs.io/en/master/modules/ppo.html)
    - labml implementation of PPO: [GitHub](https://github.com/labmlai/annotated_deep_learning_paper_implementations/tree/master/labml_nn/rl/ppo),
      [Helpers](https://github.com/labmlai/labml/tree/master/helpers/labml_helpers)
    - pytorch-a2c-ppo-acktr: [`ppo.py`](https://github.com/ikostrikov/pytorch-a2c-ppo-acktr-gail/blob/master/a2c_ppo_acktr/algo/ppo.py)
    - Anand's implementation of SAC: [GitLab](https://gitlab.inria.fr/aballou/actor-critic-algorithm/-/tree/master)
    - The 32 Implementation Details of PPO: article [link](https://costa.sh/blog-the-32-implementation-details-of-ppo.html)
    - [CleanRL](https://github.com/vwxyzjn/cleanrl/tree/master) (single file RL algorithms implementations)
    - [Tienshou](https://github.com/thu-ml/tianshou) (PyTorch based RL lib focusing on simplicity): [`ppo.py`](https://github.com/thu-ml/tianshou/blob/master/tianshou/policy/modelfree/ppo.py)
    - Lucas Willems' PPO: [`ppo.py`](https://github.com/lcswillems/torch-ac/blob/master/torch_ac/algos/ppo.py)
- RNN:
    - YouTube tutorials:
        - [PyTorch RNN Tutorial - Name Classification Using A Recurrent Neural Net](https://www.youtube.com/watch?v=WEV61GmmPrk)
        - [PyTorch Tutorial - RNN & LSTM & GRU - Recurrent Neural Nets](https://www.youtube.com/watch?v=0_PgWWmauHk)
        - Code: [GitHub](https://github.com/python-engineer/pytorch-examples/tree/master/rnn-lstm-gru)
    - How to apply LSTM using PyTorch: [article](https://cnvrg.io/pytorch-lstm/)
    - How to correctly give inputs to Embedding, LSTM and Linear layers in PyTorch?
      [StackOverFlow](https://stackoverflow.com/questions/49466894/how-to-correctly-give-inputs-to-embedding-lstm-and-linear-layers-in-pytorch/49473068#49473068)
    - Taming LSTMs: Variable-sized mini-batches: [Medium](https://towardsdatascience.com/taming-lstms-variable-sized-mini-batches-and-why-pytorch-is-good-for-your-health-61d35642972e)
    - [A PyTorch RNN with variable sequence lengths](https://vl8r.eu/posts/2019/11/23/a-pytorch-rnn-with-variable-sequence-lengths/) by Vincent Lequertier
    - PyTorch padding: [`pad_packed_sequence`](https://pytorch.org/docs/stable/generated/torch.nn.utils.rnn.pad_packed_sequence.html#torch.nn.utils.rnn.pad_packed_sequence)
[`pack_padded_sequence`](https://pytorch.org/docs/stable/generated/torch.nn.utils.rnn.pack_padded_sequence.html#torch.nn.utils.rnn.pack_padded_sequence)
- Variable length input in RL:
    - [DynEnv](https://github.com/szemenyeim/DynEnv#the-observation-space)
    - How to deal with different state space size in reinforcement learning? [StackOverFlow](https://stackoverflow.com/questions/63728800/how-to-deal-with-different-state-space-size-in-reinforcement-learning)
- PyTorch:
    - [Saving and Loading Models](https://pytorch.org/tutorials/beginner/saving_loading_models.html)
    - [Multiprocessing best practices](https://pytorch.org/docs/stable/notes/multiprocessing.html)
    - [PyTorch Performance Tuning Guide](https://www.youtube.com/watch?v=9mS1fIYj1So)
    - [Nvidia APEX (mixed precision training)](https://github.com/NVIDIA/apex)
- Audio:
    - scipy [doc](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.stft.html)
    - nice guide on STFT: [link](https://www.audiolabs-erlangen.de/resources/MIR/FMP/C2/C2_STFT-Basic.html)

### TODO:
- test policy every N iterations.
