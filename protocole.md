# Reinforcement learning for audio improving navigation

## Problem statement and objective

Hypotheses:
* The agent can **move freely** in the environment
* The **ASR system** is a black box and is simply used to evaluate the agent's positioning.



## Draft of the experimental architecture

* RIR are precomputed
* At run-time (training), RIR get interpolated _(Mehrotra  et al.)_

## Choice of the framework

### Automatic Speech Recognition (ASR) system: [Kaldi](http://kaldi-asr.org/)
### RIR generation:
* [PyRoomAcoustics](https://github.com/LCAV/pyroomacoustics):\
    **Pros:** Python API, nice _object oriented_ implementation\
    **Cons:** Slow implementation ?
* [gpuRIR](https://github.com/DavidDiazGuerra/gpuRIR):\
    **Pros:** Python API, very fast (10x)

At this point, we consider two RIR generation libraries.\
**PyRoomAcoustics** seems to be a commonly used RIR generation library. It provides extensive functionnality. The documentation mentions:
* Intuitive Python object-oriented interface to quickly construct different simulation scenarios involving multiple sound sources and microphones in 2D and 3D rooms;
* Fast C++ implementation of the image source model and ray tracing for general polyhedral rooms to efficiently generate room impulse responses and simulate the propagation between sources and receivers;
* Reference implementations of popular algorithms for STFT, beamforming, direction finding,
adaptive filtering, source separation, and single channel denoising.

The other alternative is **gpuRIR** which is also an implementation of the image source model.
The main difference with PyRoomAcoustics is that the algorithm is running on GPU which significantly enhance its performance.


### **Reinforcement Learning**:

It is not clear at this stage whether a continuous or discrete action space will be considered.

## Experimental protocole

An **agent** is equipped with an array of microphones (either binaural or 4-microphones array such as Ari's).
The agent is evolving in a 'shoebox' room environment.
A target speaker is randomly positioned in the room and speaks.

The goal for the agent is to dynamically position itself to better grasp the audio of a target speaker.
More precisely, the agent is trying to optimize its position to get the lowest possible WER on its ASR system.

The room is spatially discretized to form a grid of positions. The $[0, 2\pi]$ interval will also be discretized. At each sampled location and orientation, the RIR will be precomputed.
During the training of the learning algorithm, the signal heard by the agent will be computed by interpolating precomputed RIR.

The RL framework is, for now, imagined to have a discrete action space.
However, to help learning, we might add some noise to the computed movement.


## TODO list

- [ ] Write a clear specification of the I/0 for the different blocks
- [ ] Set up the **evaluation block** (running kaldi in inference mode)
- [ ] Set up the **simulation block**: From a room definition, generate a spatial grid and simulate the RIR for the various positions and orientations.
- [ ] At this point, evaluate the problem specificities: impact of the positioning on the RIR score, _hardness_ of the problem...
- [ ] Choose and implement an **interpolation solution**: from any given position and orientation, generate the according RIR by interpolating precomputed RIRs from the grid points.
- [ ] Implement a basic RL approach in a simple setting (1 target speaker + _easy_ noise).
